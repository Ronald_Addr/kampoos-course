<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::get('customers', [CustomersController::class, 'index']);
Route::resource('customers', CustomersController::class);
Route::resource('category', CategoryController::class);
Route::resource('products', ProductController::class);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

