/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue').default;
import Vue from 'vue'
import VueRouter from 'vue-router'
import Create from './components/Create.vue'
import List from './components/List.vue'
import Category from './components/Category.vue'
import CreateCategory from './components/CreateCategory.vue'
import Products from './components/Products.vue'
import CreateProducts from './components/CreateProducts.vue'
import Login from './components/Login.vue'
Vue.use(VueRouter)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const routes = [
    {path: '/create/:methodName', name: 'create', component: Create},
    {path: '/create/:methodName', name: 'createCategory', component: CreateCategory},
    {path: '/create/:methodName', name: 'createProducts', component: CreateProducts},
    {path: '/list', name: 'list', component: List},
    {path: '/category', name: 'category', component: Category},
    {path: '/products', name: 'products', component: Products},
    {path: '/update/:id/:methodName', name: 'update', component: Create},
    {path: '/update/:id/:methodName', name: 'updateCategory', component: CreateCategory},
    {path: '/update/:id/:methodName', name: 'updateProducts', component: CreateProducts},

]


const router = new VueRouter({
    routes
})

const app = new Vue({
    el: '#app',
    router,
});
