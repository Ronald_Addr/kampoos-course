@extends('layouts.auth', ['title' => 'login'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><img src="{{ asset('assets/img/company.png') }}" style="width: 80px" ></div>
                <div class="card-body">
                    @if(session('status'))
                    <div class="alert alert-success" role="alert">
                        {{  session('status') }}
                    </div>
                    @endif
                    <div class="text-center"><h1 class="h5 text-gray-900 mb-3">Login Admin</h1></div>
<!--form -->
<form method="POST" action="{{ route('login') }}">
@csrf
<div class="form-group">
</div>  
</form>
<!--form -->                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
