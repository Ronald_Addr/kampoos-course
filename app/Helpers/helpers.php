<?php

    if(! function_exists('moneyformat')){
        /**
        * @param mixed $str
        * @return void
        */
    function moneyformat($str){
        return 'Rp. ' . number_format($str,'0','','.');
    }
}

if(! function_exists('dateID')){
        /**
        * @param mixed $date
        * @return void
        */    
    function dateID($date){
        $value = Carbon\Carbon::parse($date);
        $parse = $value->locale('id');
        return $parse->translatedFormat('l, d F Y');
    }
}
?>