<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Foundation\Auth\User as Authenticatable;

class Customers extends Model
{
    //use HasFactory;
    protected $guarded = [];
    protected $table = 'customers';    
 
    // protected $hidden = [
    //     'remember_token',
    // ];

    public function invoices(){
        return $this->hasMany(Invoice::class);
    }

    public function carts(){
        return $this->hasMany(Cart::class);
    }

}
