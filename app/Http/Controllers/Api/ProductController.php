<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $data = Product::with([]);
        return response()->json([
            'status' => true,
            'data' => $data->get()
        ]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $product       = new Product();
        $product->title = $request->get('title');
        $product->slug = $request->get('slug');
        $product->category_id = $request->get('category_id');
        $product->content = $request->get('content');
        $product->image = $request->get('image');
        $product->weight= $request->get('weight');
        $product->price= $request->get('price');
        $product->save();
        return response()->json([
            'status' => true,
            'data' => $product
        ]);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
        $product = Product::find($id);
        return response()->json([
            'status' => true,
            'data' => $product
        ]);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $product = Product::find($id);
        $product->title = $request->get('title');
        $product->slug = $request->get('slug');
        $product->category_id = $request->get('category_id');
        $product->content = $request->get('content');
        $product->image = $request->get('image');
        $product->weight= $request->get('weight');
        $product->price= $request->get('price');
        $product->save();        
        return response()->json([
            'status' => true,
            'data' => $product
        ]);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product = Product::find($id);
        $product->delete();        
        return response()->json([
            'status' => true,
            'data' => $product
        ]);        
    }
}
